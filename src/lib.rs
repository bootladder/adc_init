use std::process::Command; use std::mem; 
const SERIAL_DEVICE_PATH: &'static str = "/dev/ttyS4" ;
////////////////////////////////////
const ADDRESS_OF_FUNIT_READ32: &'static str = "0x2300";
const ADDRESS_OF_FUNIT_READ16: &'static str = "0x2400";
const ADDRESS_OF_FUNIT_READ8: &'static str = "0x2500";
const ADDRESS_OF_FUNIT_WRITE32: &'static str = "0x2000";
const ADDRESS_OF_FUNIT_WRITE16: &'static str = "0x2100";
const ADDRESS_OF_FUNIT_WRITE8: &'static str = "0x2200";
const ADDRESS_OF_FUNIT_GCLK5OSC8ADC_INIT: &'static str = "0x3000";
const ADDRESS_OF_FUNIT_ADCREAD: &'static str = "0x3100";
////////////////////////////////////


////////////////////////////////////
const ADC_BASE_ADDR: u32 = 0x42002000;
const ADC_CTRLA_OFFSET_BYTES: u8 = 0x00;
const ADC_REFCTRL_OFFSET_BYTES: u8 = 0x01;
const ADC_STATUS_OFFSET_BYTES: u8 = 0x19;
const ADC_INPUTCTRL_OFFSET_BYTES: u8 = 0x10;
const ADC_SWTRIG_OFFSET_BYTES: u8 = 0x0C;
const ADC_RESULT_OFFSET_BYTES: u8 = 0x1A;
const ADC_SAMPCTRL_OFFSET_BYTES: u8 = 0x03;
const ADC_INTFLAG_OFFSET_BYTES: u8 = 0x18;
////////////////////////////////////
const SYSCTRL_BASE_ADDR: u32 = 0x40000800;
const SYSCTRL_VREF_OFFSET_BYTES: u8 = 0x40;
////////////////////////////////////
const GCLK_BASE_ADDR: u32 =0x42000C00;
const GCLK_CLKCTRL_OFFSET_BYTES: u8 = 0x02;
const GCLK_GENCTRL_OFFSET_BYTES: u8 = 0x04;
const GCLK_GENDIV_OFFSET_BYTES: u8 = 0x08;
////////////////////////////////////
const PORT_BASE_ADDR: u32 =0x41004400;
const PORT_PMUX1_OFFSET_BYTES: u8 = 0x31;
const PORT_PMUX7_OFFSET_BYTES: u8 = 0x37;
////////////////////////////////////
const PM_BASE_ADDR: u32      = 0x42000C00;

pub fn adc_init_complete() {

  let mut regbuf : [u8;4] = [0,0,0,0];

 // println!("---main: RAM TEST()!");
 // ram_test();

  println!("---main: Enable GCLK5 from OSC8, and GCLK_ADC()!");
  enable_gclk5_osc8_gclk_adc();
  
  println!("---main: Enabling TEMPERATURE SENSOR()!");
  enable_temperature_sensor_and_bandgapref();

  println!("---main: setting sampctrl()!");
  set_adc_sampctrl();

  println!("---main: Setting PORT PA02 TO MUX B, ADC()!");
  set_portpins_to_mux_adc();

  println!("---main: Setting ADC Negative Input to GND()!");
  set_adc_input_tempsensor();

  println!("---main: Enabling ADC()!");
  enable_adc();

  println!("---main: CALLING ADCREAD FUNIT");
  call_adcread_funit();

  println!("---main: CALLING ADCREAD FUNIT");
  call_adcread_funit();

//  println!("---main: STARTING ADC CONVERSION!");
//  start_adc_conversion(); 
//  
//  println!("---main: READING ADC RESULT!");
//  read_adc_result(); 
//
//  println!("---main: STARTING ADC CONVERSION!");
//  start_adc_conversion(); 
//
//  println!("---main: READING ADC RESULT!");
//  read_adc_result(); 
}

pub fn ram_test() {
  let mut regbuf : [u8;4] = [0,0,0,0];
  let mut regbuf16 : [u8;2] = [0,0];
  let mut regbuf8 : u8;
  regbuf[0] = 0x12;//outvec[0];  
  regbuf[1] = 0x23;//outvec[1];
  regbuf[2] = 0x34;//outvec[2] & 0xF0;
  regbuf[3] = 0x45;//outvec[3] | 0x2 ;
  regbuf16[0] = 0x66;
  regbuf16[1] = 0x66;
  regbuf8=0x77;

  println!("---main: writing DUMMY RAM 0x20000300 to 0x20000600 ()!");

  //let outvec = read_register_halfword(0x20000300, 0x0);
  //let outvec = write_register_halfword(regbuf);

let mut loopaddr: u32 = 0x20000D00;
let mut loopdata8: u8 = 0;

while loopaddr < 0x20000E00
{
  read_register_byte(loopaddr, 0x0);
  write_register_byte(regbuf8);
  loopaddr = loopaddr+4;
}

loopaddr = 0x20000D00;
while loopaddr < 0x20000E00
{
  let outvec = read_register_byte(loopaddr, 0x0);
  println!("byt0 == {:X}",outvec[0]);
  println!("byt1 == {:X}",outvec[1]);
  println!("byt2 == {:X}",outvec[2]);
  println!("byt3 == {:X}",outvec[3]);
  loopaddr = loopaddr+4;
}

}

////////////////////////////////////
pub fn call_adcread_funit() -> u32 {
    //Execute Funit :enable clocks 
      let output = Command::new("funit_host.out")
                  .arg("-d")
                  .arg(SERIAL_DEVICE_PATH)
                  .arg("-X")
                  .arg("-A")
                  .arg(ADDRESS_OF_FUNIT_ADCREAD)
                  .arg("-P")
                  .arg("0x0")
                  .output()
                  .expect("funit host command failed to start");

    print_bytes_vecu8_4(&output.stdout);

    let data32 : u32;
    let data_u8 : [u8;4] = [output.stdout[0],output.stdout[1],output.stdout[2],output.stdout[3]];
    
    unsafe{
      let c: u32 = mem::transmute(data_u8);
      let d = c.to_be();
      data32 = d;
    }
    return data32;
}

pub fn is_adc_enabled() -> bool {

  println!("---main: reading register CTRLA()!");
  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_CTRLA_OFFSET_BYTES);
  if (outvec[0] & 0x02) > 0
  {
    return true;
  }
    return false;

}

////////////////////////////////////
fn enable_gclk5_osc8_gclk_adc() {
    //Execute Funit :enable clocks 
      let output = Command::new("funit_host.out")
                  .arg("-d")
                  .arg(SERIAL_DEVICE_PATH)
                  .arg("-X")
                  .arg("-A")
                  .arg(ADDRESS_OF_FUNIT_GCLK5OSC8ADC_INIT)
                  .arg("-P")
                  .arg("0x0")
                  .output()
                  .expect("funit host command failed to start");

    print_bytes_vecu8_4(&output.stdout);
}

////////////////////////////////////
fn enable_temperature_sensor_and_bandgapref() {

  println!("---main: reading register VREF()!");
  print_register_contents(SYSCTRL_BASE_ADDR, SYSCTRL_VREF_OFFSET_BYTES);
  let outvec = read_register_byte(SYSCTRL_BASE_ADDR, SYSCTRL_VREF_OFFSET_BYTES);

  println!("---main: writing VREF:  0x06, TEMP and BGOUT ENABLE()!");
  let outvec = write_register_byte(0x6);
  print_register_contents(SYSCTRL_BASE_ADDR, SYSCTRL_VREF_OFFSET_BYTES);
}
////////////////////////////////////
fn set_adc_sampctrl() {
  println!("---main: reading register SAMPCTRL()!");
  read_register_byte(ADC_BASE_ADDR, ADC_SAMPCTRL_OFFSET_BYTES);
  write_register_byte(0x0F);

}
  
////////////////////////////////////
fn enable_adc() {

  println!("---main: Check if SYNCBUSY!");
  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_STATUS_OFFSET_BYTES);
  if outvec[0] != 0 
  {
    println!("---main: SYNCBUSY IS ACTIVE, RETURN");
    return;
  }
  println!("---main: SYNCBUSY NOT ACTIVE, GO");

  println!("---main: reading register CTRLA()!");
  print_register_contents(ADC_BASE_ADDR, ADC_CTRLA_OFFSET_BYTES);

  println!("---main: writing ADC ::  REFCTRL!");
  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_REFCTRL_OFFSET_BYTES);
  let outvec = write_register_byte(0x80);

  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_STATUS_OFFSET_BYTES);
  if outvec[0] != 0 
  {
    println!("---main: SYNCBUSY IS ACTIVE, RETURN");
    return;
  }

  println!("---main: writing ADC ::  CTRLA, ENABLE ADC!");
  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_CTRLA_OFFSET_BYTES);
  let outvec = write_register_byte(0x2);

  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_STATUS_OFFSET_BYTES);
  if outvec[0] != 0 
  {
    println!("---main: SYNCBUSY IS ACTIVE, RETURN");
    return;
  }

  println!("---main: reading register CTRLA()!");
  print_register_contents(ADC_BASE_ADDR, ADC_CTRLA_OFFSET_BYTES);

  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_STATUS_OFFSET_BYTES);
  println!("---main: read SYNCBUSY =====  {:X}",outvec[0]);
}

////////////////////////////////////
fn read_adc_result() {

  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_STATUS_OFFSET_BYTES);
  if outvec[0] != 0 
  {
    println!("---main: SYNCBUSY IS ACTIVE, RETURN");
    return;
  }

  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_INTFLAG_OFFSET_BYTES);
  if  (outvec[0] & 0x01) == 0 
  {
    println!("---main: RESRDY == 0, RETURN");
    return;
  }

  println!("---main: SYNCBUSY CLEAR, RESRDY SET, GO");

  println!("---main: reading ADC RESULT()!");
  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_RESULT_OFFSET_BYTES);

  println!("RESULT[0] = 0x{:X}",outvec[0]);

  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_RESULT_OFFSET_BYTES+1);
  println!("RESULT[1] = 0x{:X}",outvec[1]);
}

////////////////////////////////////
fn start_adc_conversion() {
  
  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_STATUS_OFFSET_BYTES);
  if outvec[0] != 0 
  {
    println!("---main: SYNCBUSY IS ACTIVE, RETURN");
    return;
  }

  println!("---main: SYNCBUSY CLEAR, GO");
  println!("---main: reading ADC SWTRIG()!");
  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_SWTRIG_OFFSET_BYTES);

  println!("---main: writing ADC SWTRIG");
  let outvec = write_register_byte(0x03);

  let outvec = read_register_byte(ADC_BASE_ADDR, ADC_STATUS_OFFSET_BYTES);
  if outvec[0] != 0 
  {
    println!("---main: SYNCBUSY IS ACTIVE, RETURN");
    return;
  }
  println!("---main: SYNCBUSY CLEAR, GO");
}

////////////////////////////////////
fn set_portpins_to_mux_adc() {

  println!("---main: reading PORT PMUX 1 for PA02!");
  let outvec = read_register_byte(PORT_BASE_ADDR, PORT_PMUX1_OFFSET_BYTES);
  println!("---main: the byte contains: {:X}!", outvec[0]);
  let outvec = write_register_byte(0x01); //PMUX B
  let outvec = read_register_byte(PORT_BASE_ADDR, PORT_PMUX1_OFFSET_BYTES);
  println!("---main: the byte contains: {:X}!", outvec[0]);


  println!("---main: reading PORT PMUX 7 for PA14!");
  let outvec = read_register_byte(PORT_BASE_ADDR, PORT_PMUX7_OFFSET_BYTES);
  println!("---main: the byte contains: {:X}!", outvec[0]);
  let outvec = write_register_byte(0x01); //PMUX B
  let outvec = read_register_byte(PORT_BASE_ADDR, PORT_PMUX7_OFFSET_BYTES);
  println!("---main: the byte contains: {:X}!", outvec[0]);
}

////////////////////////////////////
pub fn set_adc_input_tempsensor() {
  let mut regbuf : [u8;4] = [0,0,0,0];

  println!("---main: reading ADC INPUTCTRL()!");
  let outvec = read_register(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);

  regbuf[0] = 0x00;
  regbuf[1] = 0x00;
  regbuf[2] = 0x19;
  //regbuf[3] = 0x00;
  regbuf[3] = 0x18;
  let outvec = write_register(regbuf);

  print_register_contents(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);
}

////////////////////////////////////
pub fn set_adc_input_ain0_pa02() {
  let mut regbuf : [u8;4] = [0,0,0,0];

  println!("---main: reading ADC INPUTCTRL()!");
  read_register(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);

  regbuf[0] = 0x00;
  regbuf[1] = 0x00;
  regbuf[2] = 0x19;
  //regbuf[3] = 0x00;
  regbuf[3] = 0x0;
  let outvec = write_register(regbuf);

  print_register_contents(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);
}

////////////////////////////////////
pub fn set_adc_input_ain7_pa15() {
  let mut regbuf : [u8;4] = [0,0,0,0];

  println!("---main: reading ADC INPUTCTRL()!");
  let outvec = read_register(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);

  regbuf[0] = 0x00;
  regbuf[1] = 0x00;
  regbuf[2] = 0x19;
  //regbuf[3] = 0x00;
  regbuf[3] = 0x7;
  let outvec = write_register(regbuf);

  print_register_contents(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);
}

////////////////////////////////////
pub fn set_adc_input_ain6_pa14() {
  let mut regbuf : [u8;4] = [0,0,0,0];

  println!("---main: reading ADC INPUTCTRL()!");
  let outvec = read_register(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);

  regbuf[0] = 0x00;
  regbuf[1] = 0x00;
  regbuf[2] = 0x19;
  //regbuf[3] = 0x00;
  regbuf[3] = 0x6;
  let outvec = write_register(regbuf);

  print_register_contents(ADC_BASE_ADDR, ADC_INPUTCTRL_OFFSET_BYTES);
}
fn enable_gclk5_osc8m() {
  let mut regbuf : [u8;4] = [0,0,0,0];
  println!("---main: reading GENDIV()!");
  let outvec = read_register(GCLK_BASE_ADDR, GCLK_GENDIV_OFFSET_BYTES);

  println!("---main: selecting GENDIV 5()!");
  let outvec = write_register_byte(0x05);

  println!("---main: writing GENDIV()!");
  println!("0x9: GENDIV: set bit 16-8, DIV : 1");
  println!("0x8: GENDIV: set bit 3-0, ID GCLK5 : 0x05");
  regbuf[0] = outvec[0] ;
  regbuf[1] = outvec[1] ;
  regbuf[2] = 0x01;
  regbuf[3] = 0x05;
  let outvec = write_register(regbuf);

  let outvec = read_register_byte(GCLK_BASE_ADDR, GCLK_GENDIV_OFFSET_BYTES);
  //to read the register, first write to ID
  let outvec = write_register_byte(0x05);
  print_register_contents(GCLK_BASE_ADDR, GCLK_GENDIV_OFFSET_BYTES);

  println!("---main: reading GENCTRL()!");
  let outvec = read_register(GCLK_BASE_ADDR, GCLK_GENCTRL_OFFSET_BYTES);

  println!("---main: selecting GENCTRL()!");
  let outvec = write_register_byte(0x05);

  println!("---main: writing GENCTRL()!");
  println!("0x4: GENCTRL: set bit 16, GENEN!");
  println!("0x4: GENCTRL: set bit 15-8, SRC OSC8M : 0x06");
  println!("0x4: GENCTRL: set bit 7-0, ID  GCLK5: 0x05");
  regbuf[0] = outvec[0] ;
  regbuf[1] = outvec[1] | 0x01;
  regbuf[2] = 0x06;
  regbuf[3] = 0x05;
  let outvec = write_register(regbuf);

  print_register_contents(GCLK_BASE_ADDR, GCLK_GENCTRL_OFFSET_BYTES);
  read_register(GCLK_BASE_ADDR, GCLK_GENCTRL_OFFSET_BYTES);
  print_bytes_vecu8_4(&outvec);
}
//fn enable_gclk_adc() {
//  let mut regbuf16 : [u8;2] = [0,0];
//  println!("---main: reading CLKCTRL()!");
//  let outvec = read_register_halfword(GCLK_BASE_ADDR, GCLK_CLKCTRL_OFFSET_BYTES);
//
//  println!("---main: writing GCLK_ADC()!");
//  println!("0x3: CLKCTRL: set bit 14, CLKEN!");
//  println!("0x3: CLKCTRL: set bit 11-8, GEN5: 0x05");
//  println!("0x2: CLKCTRL: set bit 7-0, ID GCLK_ADC: 0x13");
//  regbuf16[0] = 0x45;
//  regbuf16[1] = 0x13;
//  let outvec = write_register_halfword(regbuf16);
//  print_register_contents(GCLK_BASE_ADDR, 0);
//}
/*   write register 



*/
fn write_register(data: [u8;4])  ->  Vec<u8> {
    //Convert the input Vec<u8> into a u32
    let data32 : u32;
    let data_u8 : [u8;4] = [data[0],data[1],data[2],data[3]];
    
    unsafe{
      let c: u32 = mem::transmute(data_u8);
      let d = c.to_be();
      data32 = d;
    }

    //Execute Funit : write32 
      let output = Command::new("funit_host.out")
                  .arg("-d")
                  .arg(SERIAL_DEVICE_PATH)
                  .arg("-X")
                  .arg("-A")
                  .arg(ADDRESS_OF_FUNIT_WRITE32)
                  .arg("-P")
                  .arg(format!("0x{:X}", data32))
                  .output()
                  .expect("funit host command failed to start");

    return output.stdout;
}
//fn write_register_halfword(data: [u8;2])  ->  Vec<u8> {
//    //Convert the input Vec<u8> into a u32
//    let data16 : u16;
//    let data_u8 : [u8;2] = [data[0],data[1]];
//    
//    unsafe{
//      let c: u16 = mem::transmute(data_u8);
//      let d = c.to_be();
//      data16 = d;
//    }
//
//    
//    //Execute Funit : write32 
//      let output = Command::new("funit_host.out")
//                  .arg("-d")
//                  .arg(SERIAL_DEVICE_PATH)
//                  .arg("-X")
//                  .arg("-A")
//                  .arg(ADDRESS_OF_FUNIT_WRITE16)
//                  .arg("-P")
//                  .arg(format!("0x{:X}", data16))
//                  .output()
//                  .expect("funit host command failed to start");
//
//    return output.stdout;
//}
fn write_register_byte(data: u8)  ->  Vec<u8> {
    
    //Execute Funit : write32 
      let output = Command::new("funit_host.out")
                  .arg("-d")
                  .arg(SERIAL_DEVICE_PATH)
                  .arg("-X")
                  .arg("-A")
                  .arg(ADDRESS_OF_FUNIT_WRITE8)
                  .arg("-P")
                  .arg(format!("0x{:X}", data))
                  .output()
                  .expect("funit host command failed to start");

    return output.stdout;
}

/*   read register 


*/
fn read_register( baseaddr: u32, offset:u8 ) -> Vec<u8>  {

    let aligned_address : u32 = (baseaddr+ (offset as u32) ) & !(0x00000003);
    //Execute Funit : read32
    let output = Command::new("funit_host.out")
                .arg("-d")
                .arg(SERIAL_DEVICE_PATH)
                .arg("-X")
                .arg("-A")
                .arg(ADDRESS_OF_FUNIT_READ32)
                .arg("-P")
                .arg(format!("0x{:X}", aligned_address))
                .output()
                .expect("funit host command failed to start");

  return output.stdout;
}
//fn read_register_halfword( baseaddr: u32, offset:u8 ) -> Vec<u8>  {
//
//    let aligned_address : u32 = (baseaddr+ (offset as u32) ) & !(0x00000001);
//    //Execute Funit : read16
//    let output = Command::new("funit_host.out")
//                .arg("-d")
//                .arg(SERIAL_DEVICE_PATH)
//                .arg("-X")
//                .arg("-A")
//                .arg(ADDRESS_OF_FUNIT_READ16)
//                .arg("-P")
//                .arg(format!("0x{:X}", aligned_address))
//                .output()
//                .expect("funit host command failed to start");
//
//  return output.stdout;
//}
fn read_register_byte( baseaddr: u32, offset:u8 ) -> Vec<u8>  {

    let aligned_address : u32 = (baseaddr+ (offset as u32) );
    //Execute Funit : read16
    let output = Command::new("funit_host.out")
                .arg("-d")
                .arg(SERIAL_DEVICE_PATH)
                .arg("-X")
                .arg("-A")
                .arg(ADDRESS_OF_FUNIT_READ8)
                .arg("-P")
                .arg(format!("0x{:X}", aligned_address))
                .output()
                .expect("funit host command failed to start");

  return output.stdout;
}
fn print_register_contents( baseaddr: u32, offset:u8 )  {

    let outvec = read_register( baseaddr, offset);
    print_bytes_vecu8_4(&outvec);
}


fn print_bytes_vecu8_4(data: &Vec<u8>) {

  println!("1st byte MSB {:02X}   ",data[0]);
  println!("byte         {:02X}   ",data[1]);
  println!("byte         {:02X}   ",data[2]);
  println!("4th byte LSB {:02X}   ",data[3]);
}



  //println!("---main: Enabling GCLK5 from OSC8M()!");
  //enable_gclk5_osc8m();
  //println!("---main: Enabling GCLK_ADC()!");
  //enable_gclk_adc();


//    println!("main: reading dummy RAM()!");
//
//    let outvec = read_dummy_ram();
//
//  println!("1st byte MSB {:02X}   ",outvec[0]);
//  println!("byte         {:02X}   ",outvec[1]);
//  println!("byte         {:02X}   ",outvec[2]);
//  println!("4th byte LSB {:02X}   ",outvec[3]);
//
//    println!("main: reading dummy RAM()!");
//
//    let outvec = read_dummy_ram();
//
//  println!("1st byte MSB {:02X}   ",outvec[0]);
//  println!("byte         {:02X}   ",outvec[1]);
//  println!("byte         {:02X}   ",outvec[2]);
//  println!("4th byte LSB {:02X}   ",outvec[3]);
//
//
//    println!("main: writing dummy RAM()!");
//    //Left is MSB.  Right is bit zero.  Left goes first.
//    //Left is at the lowest address.  This is big endian.
//    let outvec = write_dummy_ram([ 0x43, 0x32, 0x21, 0x10 ]);
//
//  println!("1st byte MSB {:02X}   ",outvec[0]);
//  println!("byte         {:02X}   ",outvec[1]);
//  println!("byte         {:02X}   ",outvec[2]);
//  println!("4th byte LSB {:02X}   ",outvec[3]);



  //get the output from stdout, as a Sring
    //let thestring = String::from_utf8_lossy(&output.stdout);
   // for &byte in thestring.as_bytes() {
   //   print!("{:02X}",byte);
   // }

  //let return32 : u32 = thestring.parse().unwrap();
  //let outvec = read_register_halfword(0x20000304, 0x0);
  //let outvec = write_register_halfword(regbuf16);
  //let outvec = read_register_halfword(0x20000306, 0x0);
  //let outvec = write_register_halfword(regbuf16);

  //let outvec = read_register_byte(0x20000308, 0x0);
  //let outvec = write_register_byte(regbuf8);
  //let outvec = read_register_byte(0x20000309, 0x0);
  //let outvec = write_register_byte(regbuf8);
  //let outvec = read_register_byte(0x2000030A, 0x0);
  //let outvec = write_register_byte(regbuf8);
  //let outvec = read_register_byte(0x2000030B, 0x0);
  //let outvec = write_register_byte(regbuf8);

  //let outvec = read_register(0x20000300, 0x0);
  //print_bytes_vecu8_4(&outvec);
  //let outvec = read_register(0x20000304, 0x0);
  //print_bytes_vecu8_4(&outvec);
  //let outvec = read_register(0x20000308, 0x0);
  //print_bytes_vecu8_4(&outvec);
